{
    'name': "HR Extension",
    'version': '1.0',
    'depends': ['hr', 'hr_contract', 'mail', 'contacts'],
    'author': "BS-23",
    'category': 'Category',
    'description': """
    Description text
    """,
    'data': [
        'views/res_partner_views.xml',
        'views/permanent_address_form.xml',
        'data/employee_contract_notification.xml',
        'data/contract_reminder_email_template.xml'
    ],
    'application': True,
    'license': 'LGPL-3',
}
