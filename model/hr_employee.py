# -*- coding: utf-8 -*-

from odoo import fields, models
from bs_lib.service import app_constant
from dateutil.relativedelta import relativedelta
from datetime import date


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _description = 'Employee'

    employee_type_selection = [
        ('regular', 'Regular'),
        ('probationer', 'Probationer'),
        ('contractual', 'Contractual'),
        ('trainee', 'Trainee'),
        ('advisor', 'Advisor')
    ]

    birth_certificate_number = fields.Char("Birth Certificate Number")
    blood_group = fields.Selection(selection=app_constant.blood_group_selection, string="Blood Group")
    employee_types = fields.Selection(selection=employee_type_selection, string="Employee Type")

    permanent_address_id = fields.Many2one(
        'res.partner', 'Permanent Address', help='Enter here the permanent address of the employee.',
        groups="hr.group_hr_user", tracking=True,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    def contract_ending_reminder(self, after_days=15):
        contract_end_check = fields.Date.today() + relativedelta(days=after_days)
        contracts_expire_soon_records = self.env['hr.employee'].search([('contract_ids.date_end', '<=', contract_end_check), ('contract_ids.date_end', '>', fields.Date.today())])
        template_id = self.env.ref('hr_ext.contract_reminder_email_template').id
        for record in contracts_expire_soon_records:
            self.env['mail.template'].browse(template_id).send_mail(record.id, force_send=True)
