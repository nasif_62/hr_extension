# -*- coding: utf-8 -*-

from odoo import models, fields, api

PERMANENT_ADDRESS_FIELDS = ('permanent_house_no', 'permanent_road_no', 'permanent_area', 'permanent_district', 'permanent_division', 'permanent_country_id')

ADDRESS_FIELDS = ('present_house_no', 'street', 'present_area', 'present_district', 'present_division', 'country_id')


@api.model
def _lang_get(self):
    return self.env['res.lang'].get_installed()


class ResPartner(models.Model):
    _inherit = 'res.partner'

    permanent_house_no = fields.Char(string="House Number")
    permanent_road_no = fields.Char(string="Road Number")
    permanent_area = fields.Char(string="Area")
    permanent_post_office = fields.Char(string="Post Office")
    permanent_police_station = fields.Char(string="Police Station")
    permanent_district = fields.Char(string="District")
    permanent_division = fields.Char(string="Division")
    permanent_city = fields.Char(string="City")
    permanent_country_id = fields.Many2one('res.country', string='Country')
    permanent_zip_code = fields.Char(string="Zip Code")
    permanent_phone = fields.Char(string="Phone")
    permanent_mobile = fields.Char(string="Mobile")
    permanent_email = fields.Char(string="Email")
    permanent_lang = fields.Selection(_lang_get, string='Language')

    present_house_no = fields.Char(string="House Number")
    present_area = fields.Char(string="Area")
    present_post_office = fields.Char(string="Post Office")
    present_police_station = fields.Char(string="Police Station")
    present_district = fields.Char(string="District")
    present_division = fields.Char(string="Division")
    present_city = fields.Char(string="City")

    @api.depends('is_company', 'name', 'parent_id.display_name', 'type', 'company_name')
    def _compute_display_name(self):
        diff = dict(show_address=None, show_permanent_address=None, show_address_only=None, show_email=None, html_format=None, show_vat=None)
        names = dict(self.with_context(**diff).name_get())
        for partner in self:
            partner.display_name = names.get(partner.id)

    @api.model
    def _permanent_address_fields(self):
        """Returns the list of address fields that are synced from the parent."""
        return list(PERMANENT_ADDRESS_FIELDS)

    @api.model
    def _address_fields(self):
        """Returns the list of address fields that are synced from the parent."""
        return list(ADDRESS_FIELDS)

    @api.model
    def _formatting_permanent_address_fields(self):
        """Returns the list of address fields usable to format addresses."""
        return self._permanent_address_fields()

    @api.model
    def _get_address_format(self):
        return self._get_default_address_format()

    @api.model
    def _get_default_permanent_address_format(self):
        return "%(permanent_house_no)s\n%(permanent_road_no)s\n%(permanent_area)s \n%(permanent_district)s \n%(permanent_division)s\n%(permanent_country_name)s"

    @api.model
    def _get_default_address_format(self):
        return "%(present_house_no)s\n%(street)s\n%(present_area)s\n%(present_district)s\n%(present_division)s\n%(country_name)s"

    @api.model
    def _get_permanent_address_format(self):
        return self._get_default_permanent_address_format()

    def _prepare_display_address(self, without_company=False):
        # get the information that will be injected into the display format
        # get the address format
        address_format = self._get_address_format()
        args = {
            'present_house_no': self.present_house_no or '',
            'street': self.street or '',
            'present_area': self.present_area or '',
            'present_district': self.present_district,
            'present_division': self.present_division,
            'country_name': self._get_country_name() or '',
        }
        for field in self._formatting_address_fields():
            args[field] = getattr(self, field) or ''
        if without_company:
            args['company_name'] = ''
        elif self.commercial_company_name:
            address_format = '%(company_name)s\n' + address_format
        return address_format, args

    def _prepare_display_permanent_address(self):
        # get the information that will be injected into the display format
        # get the address format
        address_format = self._get_permanent_address_format()
        args = {
            'permanent_house_no': self.permanent_house_no or '',
            'permanent_road_no': self.permanent_road_no or '',
            'permanent_area': self.permanent_area or '',
            'permanent_district': self.permanent_district or '',
            'permanent_division': self.permanent_division or '',
            'permanent_country_name': self._get_permanent_country_name(),
        }
        for field in self._formatting_permanent_address_fields():
            args[field] = getattr(self, field) or ''

        return address_format, args

    def _get_permanent_country_name(self):
        return self.permanent_country_id.name or ''

    def _display_permanent_address(self):
        '''
        The purpose of this function is to build and return an address formatted accordingly to the
        standards of the country where it belongs.

        :param without_company: if address contains company
        :returns: the address formatted in a display that fit its country habits (or the default ones
            if not country is specified)
        :rtype: string
        '''
        address_format, args = self._prepare_display_permanent_address()
        return address_format % args

    def _get_name(self):
        """ Utility method to allow name_get to be overrided without re-browse the partner """
        partner = self
        name = partner.name or ''

        if partner.company_name or partner.parent_id:
            if not name and partner.type in ['invoice', 'delivery', 'other']:
                name = dict(self.fields_get(['type'])['type']['selection'])[partner.type]
            if not partner.is_company:
                name = self._get_contact_name(partner, name)
        if self._context.get('show_address_only'):
            name = partner._display_address(without_company=True)
        if self._context.get('show_address'):
            name = name + "\n" + partner._display_address(without_company=True)
        name = name.replace('\n\n', '\n')
        name = name.replace('\n\n', '\n')
        if self._context.get('show_permanent_address'):
            name = name + "\n" + partner._display_permanent_address()
        name = name.replace('\n\n', '\n')
        name = name.replace('\n\n', '\n')
        if self._context.get('partner_show_db_id'):
            name = "%s (%s)" % (name, partner.id)
        if self._context.get('address_inline'):
            splitted_names = name.split("\n")
            name = ", ".join([n for n in splitted_names if n.strip()])
        if self._context.get('show_email') and partner.email:
            name = "%s <%s>" % (name, partner.email)
        if self._context.get('html_format'):
            name = name.replace('\n', '<br/>')
        if self._context.get('show_vat') and partner.vat:
            name = "%s ‒ %s" % (name, partner.vat)
        return name


